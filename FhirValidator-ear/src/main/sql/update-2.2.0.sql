INSERT INTO app_configuration(id, value, variable) VALUES (nextval('app_configuration_id_seq'), '/opt/fhirvalidator/CodeSystem', 'code_system_location');
alter table fhir_validator_description add column dtype varchar(31);
update fhir_validator_description set dtype = 'resource_val' where format is not null;
update fhir_validator_description set dtype = 'url_val' where format is null;
DELETE FROM fhir_validator_description where format = 0;
UPDATE fhir_validator_description set name = replace(name, ' XML', '');
ALTER TABLE fhir_validator_description DROP COLUMN format;


