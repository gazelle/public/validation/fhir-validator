<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <parent>
        <groupId>net.ihe.gazelle</groupId>
        <artifactId>external-library</artifactId>
        <version>1.0</version>
    </parent>

    <modelVersion>4.0.0</modelVersion>
    <groupId>net.ihe.gazelle.validator</groupId>
    <artifactId>FhirValidator</artifactId>
    <packaging>pom</packaging>
    <version>4.1.9-SNAPSHOT</version>
    <name>FhirValidator</name>

    <scm>
        <connection>scm:git:${git.project.url}</connection>
        <url>scm:git:${git.project.url}</url>
        <developerConnection>scm:git:${git.project.url}</developerConnection>
        <tag>FhirValidator-4.1.3</tag>
    </scm>

    <organization>
        <name>IHE</name>
        <url>http://www.ihe.net/</url>
    </organization>

    <developers />

    <contributors />

    <issueManagement>
        <system>Jira</system>
        <url>https://gazelle.ihe.net/jira/browse/FHIRVAL</url>
    </issueManagement>

    <properties>
        <simulator.admin.name>Developer</simulator.admin.name>
        <simulator.admin.mail>developper@ihe.net</simulator.admin.mail>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <maven.build.timestamp.format>yyyyMMddHHmmss</maven.build.timestamp.format>
        <gazelle-assets-version>${maven.build.timestamp}</gazelle-assets-version>
        <hapi.fhir.version>5.0.2</hapi.fhir.version>
        <messages.mode>crowdin</messages.mode>
        <sonar.jacoco.reportPaths>target/jacoco.exec</sonar.jacoco.reportPaths>
        <sonar.core.codeCoveragePlugin>jacoco</sonar.core.codeCoveragePlugin>
        <version.dependency.plugin>3.0.0</version.dependency.plugin>
        <version.surefire.plugin>2.20</version.surefire.plugin>
        <version.resources.plugin>3.0.2</version.resources.plugin>
        <version.compiler.plugin>3.6.1</version.compiler.plugin>
        <tool.version>4.0.0</tool.version>
        <hql.version>3.0.1</hql.version>
        <revision>4.0.0-SNAPSHOT</revision>
        <jacoco.version>0.8.1</jacoco.version>
        <sonar.maven.plugin.version>3.7.0.1746</sonar.maven.plugin.version>
        <nexus.url>https://gazelle.ihe.net/nexus</nexus.url>
        <nexus.path>/content/groups/public/</nexus.path>
        <git.user.name>gitlab-ci</git.user.name>
        <git.user.token>changeit</git.user.token>
        <git.project.url>
            https://${git.user.name}:${git.user.token}@gitlab.inria.fr/gazelle/public/validation/fhir-validator.git
        </git.project.url>
    </properties>

    <build>
        <plugins>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.8.4</version>
                <executions>
                    <execution>
                        <id>pre-unit-test</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>post-unit-test</id>
                        <phase>test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <compilerVersion>1.8</compilerVersion>
                    <fork>true</fork>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
        </plugins>
    </build>


    <profiles>
        <profile>
            <id>development</id>

            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>

            <properties>
                <basename>GazelleFhirValidator</basename>
                <!-- development mode (disable in production) -->
                <!--<seam.debug>true</seam.debug>-->
                <!-- datasource configuration -->
                <jdbc.connection.url>jdbc:postgresql:gazelle-fhir-validator</jdbc.connection.url>
                <jdbc.driver.class>org.postgresql.Driver</jdbc.driver.class>
                <jdbc.user>gazelle</jdbc.user>
                <jdbc.password>gazelle</jdbc.password>
                <min.pool.size>1</min.pool.size>
                <max.pool.size>3</max.pool.size>
                <messages.mode>crowdin</messages.mode>

                <!-- package exploded war file -->
                <exploded.war.file>true</exploded.war.file>

                <!-- development mode (exclude in production) -->
                <exclude.bootstrap>false</exclude.bootstrap>

                <!-- persistence.xml configuration -->
                <hibernate.dialect>
                    org.hibernate.dialect.PostgreSQLDialect
                </hibernate.dialect>
                <hibernate.hbm2ddl.auto>
                    validate
                </hibernate.hbm2ddl.auto>
                <hibernate.show_sql>
                    false
                </hibernate.show_sql>
            </properties>
        </profile>

        <profile>
            <id>production</id>
            <activation>
                <property>
                    <name>performRelease</name>
                    <value>true</value>
                </property>
            </activation>

            <properties>
                <basename>GazelleFhirValidator</basename>
                <!-- development mode (disable in production) -->
                <!--<seam.debug>false</seam.debug>-->
                <!-- datasource configuration -->
                <jdbc.connection.url>jdbc:postgresql:gazelle-fhir-validator</jdbc.connection.url>
                <jdbc.driver.class>org.postgresql.Driver</jdbc.driver.class>
                <jdbc.user>gazelle</jdbc.user>
                <jdbc.password>gazelle</jdbc.password>
                <min.pool.size>1</min.pool.size>
                <max.pool.size>3</max.pool.size>

                <!-- development mode (exclude in production) -->
                <exclude.bootstrap>true</exclude.bootstrap>

                <!-- package exploded war file -->
                <exploded.war.file>false</exploded.war.file>

                <!-- persistence.xml configuration -->
                <hibernate.dialect>
                    org.hibernate.dialect.PostgreSQLDialect
                </hibernate.dialect>
                <hibernate.hbm2ddl.auto>
                    validate
                </hibernate.hbm2ddl.auto>
                <hibernate.show_sql>
                    false
                </hibernate.show_sql>
            </properties>
        </profile>

        <profile>
            <id>R4</id>
            <properties>
                <basename>GazelleFhirValidatorR4</basename>
                <!-- <seam.debug>false</seam.debug>-->
                <devmode>false</devmode>
                <!-- datasource configuration -->
                <jdbc.connection.url>jdbc:postgresql:gazelle-fhir-validator-r4</jdbc.connection.url>
                <jdbc.user>gazelle</jdbc.user>
                <jdbc.password>gazelle</jdbc.password>
                <min.pool.size>1</min.pool.size>
                <max.pool.size>3</max.pool.size>
                <messages.mode>crowdin</messages.mode>
                <!-- persistence.xml configuration -->
                <hibernate.dialect>
                    org.hibernate.dialect.PostgreSQLDialect
                </hibernate.dialect>
                <hibernate.hbm2ddl.auto>
                    validate
                </hibernate.hbm2ddl.auto>
                <hibernate.show_sql>
                    false
                </hibernate.show_sql>
            </properties>
        </profile>

        <profile>
            <id>release</id>
            <activation>
                <property>
                    <name>performRelease</name>
                    <value>true</value>
                </property>
            </activation>
        </profile>

        <profile>
            <id>sonar</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.sonarsource.scanner.maven</groupId>
                        <artifactId>sonar-maven-plugin</artifactId>
                        <version>${sonar.maven.plugin.version}</version>
                        <executions>
                            <execution>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sonar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

    </profiles>

    <repositories>
        <repository>
            <id>IHE</id>
            <name>IHE Public Maven Repository Group</name>
            <url>https://gazelle.ihe.net/nexus/content/groups/public/</url>
            <layout>default</layout>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>never</updatePolicy>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>

    <distributionManagement>
        <repository>
            <id>nexus-releases</id>
            <url>https://gazelle.ihe.net/nexus/content/repositories/releases</url>
        </repository>
        <snapshotRepository>
            <id>nexus-snapshots</id>
            <url>https://gazelle.ihe.net/nexus/content/repositories/snapshots/</url>
        </snapshotRepository>
    </distributionManagement>
    <pluginRepositories>
        <pluginRepository>
            <id>IHE-plugins</id>
            <name>IHE Plugins Public Maven Repository Group</name>
            <url>https://gazelle.ihe.net/nexus/content/groups/public/</url>
            <layout>default</layout>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>never</updatePolicy>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </pluginRepository>
    </pluginRepositories>
    <modules>
        <module>Fhir-Validator-Common</module>
        <module>FhirValidator-ear</module>
        <module>Fhir-Validator-Validation</module>
        <module>Fhir-Validator-Definition</module>
    </modules>
</project>
