package net.ihe.gazelle.fhir.validator.common.adapter.dao;

import ca.uhn.fhir.rest.annotation.Transaction;
import net.ihe.gazelle.fhir.validator.common.adapter.FhirValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.adapter.FhirValidatorDescriptionQuery;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.validator.validation.model.ValidatorDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>FhirValidatorDescriptionDAO class.</p>
 *
 * @author abe
 * @version 1.0: 02/01/18
 */
@Named("fhirValidatorDescriptionTransactionManager")
public class FhirValidatorDescriptionDAO {

    @PersistenceContext(unitName = "FhirValidator-Common-PersistenceUnit")
    private static EntityManager entityManager;

    private static EntityManager getEntityManager() {
        if (entityManager == null) {
            entityManager = EntityManagerService.provideEntityManager();
        }
        return entityManager;
    }

    private static final Logger LOG = LoggerFactory.getLogger(FhirValidatorDescriptionDAO.class);

    public List<ValidatorDescription> listValidatorsForDescriminator(String descriminator) {
        if (descriminator != null && !descriminator.isEmpty()) {
            FhirValidatorDescriptionQuery query = new FhirValidatorDescriptionQuery(getEntityManager());
            query.descriminator().eq(descriminator);
            query.name().order(true);
            query.available().eq(true);
            List<FhirValidatorDescription> fhirValidators = query.getList();
            return toValidatorDescriptionList(fhirValidators);
        } else {
            return new ArrayList<ValidatorDescription>();
        }
    }

    private List<ValidatorDescription> toValidatorDescriptionList(List<FhirValidatorDescription> fhirValidators) {
        List<ValidatorDescription> validators = new ArrayList<ValidatorDescription>();
        for (FhirValidatorDescription fhirValidator : fhirValidators) {
            validators.add(fhirValidator);
        }
        return validators;
    }

    public ValidatorDescription getValidatorByOidOrName(String value) {
        FhirValidatorDescriptionQuery query = new FhirValidatorDescriptionQuery(getEntityManager());
        query.addRestriction(HQLRestrictions.or(query.name().eqRestriction(value), query.oid().eqRestriction(value)));
        return query.getUniqueResult();
    }

    @Transactional
    public FhirValidatorDescription saveFhirValidatorDescription(FhirValidatorDescription fhirValidatorDescription) {
        EntityManager entityManager = getEntityManager();
        FhirValidatorDescription merged = entityManager.merge(fhirValidatorDescription);
        entityManager.flush();
        return merged;
    }

    public FhirValidatorDescription findWithId(Class entityClass, Integer validatorId){
        return (FhirValidatorDescription) getEntityManager().find(entityClass, validatorId);
    }
}
