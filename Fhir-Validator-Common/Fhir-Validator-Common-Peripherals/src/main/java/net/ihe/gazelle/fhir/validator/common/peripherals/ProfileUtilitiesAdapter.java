package net.ihe.gazelle.fhir.validator.common.peripherals;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.context.support.IValidationSupport;
import org.hl7.fhir.r4.context.IWorkerContext;
import org.hl7.fhir.r4.hapi.ctx.HapiWorkerContext;
import org.hl7.fhir.utilities.validation.ValidationMessage;

import java.util.List;

public class ProfileUtilitiesAdapter extends org.hl7.fhir.r4.conformance.ProfileUtilities {

    public ProfileUtilitiesAdapter(FhirContext theCtx, IValidationSupport theValidationSupport, List<ValidationMessage> messages,
                                   ProfileKnowledgeProvider pkp) {
        this(new HapiWorkerContext(theCtx, theValidationSupport), messages, pkp);
    }

    public ProfileUtilitiesAdapter(IWorkerContext context, List<ValidationMessage> messages, ProfileKnowledgeProvider pkp) {
        super(context, messages, pkp);
    }
}
