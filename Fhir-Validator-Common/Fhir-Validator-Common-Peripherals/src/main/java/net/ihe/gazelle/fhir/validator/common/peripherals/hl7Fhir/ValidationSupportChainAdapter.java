package net.ihe.gazelle.fhir.validator.common.peripherals.hl7Fhir;

import ca.uhn.fhir.context.support.IValidationSupport;

public class ValidationSupportChainAdapter extends org.hl7.fhir.common.hapi.validation.support.ValidationSupportChain {

    public ValidationSupportChainAdapter(DefaultProfileValidationSupportAdapter defaultProfileValidationSupportAdapter,
                                         PrePopulatedValidationSupportAdapter prePopulatedValidationSupportAdapter) {
        super(defaultProfileValidationSupportAdapter, prePopulatedValidationSupportAdapter);
    }

    public ValidationSupportChainAdapter(IValidationSupport... theValidationSupportModules) {
        super(theValidationSupportModules);
    }
}