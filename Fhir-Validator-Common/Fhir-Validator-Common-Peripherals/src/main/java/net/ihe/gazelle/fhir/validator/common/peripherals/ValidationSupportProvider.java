package net.ihe.gazelle.fhir.validator.common.peripherals;

import ca.uhn.fhir.parser.IParser;
import net.ihe.gazelle.fhir.constants.FhirParserProvider;
import net.ihe.gazelle.fhir.context.FhirContextProvider;
import net.ihe.gazelle.fhir.validator.common.adapter.FhirResourceValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.adapter.FhirResourceValidatorDescriptionQuery;
import net.ihe.gazelle.fhir.validator.common.business.Exception.UnexpectedResourceException;
import net.ihe.gazelle.fhir.validator.common.peripherals.hapi.HapiStructureDefLoaderAdapter;
import net.ihe.gazelle.fhir.validator.common.peripherals.hl7Fhir.DefaultProfileValidationSupportAdapter;
import net.ihe.gazelle.fhir.validator.common.peripherals.hl7Fhir.PrePopulatedValidationSupportAdapter;
import net.ihe.gazelle.fhir.validator.common.peripherals.hl7Fhir.ValidationSupportChainAdapter;
import net.ihe.gazelle.preferences.PreferenceService;
import org.hl7.fhir.common.hapi.validation.support.InMemoryTerminologyServerValidationSupport;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CodeSystem;
import org.hl7.fhir.r4.model.ValueSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * <p>ValidationSupportProvider class.</p>
 *
 * @author abe
 * @version 1.0: 09/04/18
 */

public class ValidationSupportProvider {


    private static final Logger LOG = LoggerFactory.getLogger(ValidationSupportProvider.class);
    private static final DefaultProfileValidationSupportAdapter defaultProfileValidationSupportAdapter;
    private static PrePopulatedValidationSupportAdapter prePopulatedValidationSupportAdapter;
    private static final InMemoryTerminologyServerValidationSupport inMemoryTerminologyServerValidationSupport;
    private static FilenameFilter filter;
    private static int raisedExceptionCounter;

    static {
        filter = (dir, name) -> name.endsWith(".xml");
        defaultProfileValidationSupportAdapter = new DefaultProfileValidationSupportAdapter(FhirContextProvider.instance());
        loadStructureDefinitionsAndTerminologies();
        inMemoryTerminologyServerValidationSupport = new InMemoryTerminologyServerValidationSupport(FhirContextProvider.instance());
    }

    /**
     * Load Structure Definitions and Terminologies for available validators
     * @return the number of raised exceptions during the process
     */
    public static int loadStructureDefinitionsAndTerminologies() {
        raisedExceptionCounter = 0;
        prePopulatedValidationSupportAdapter = new PrePopulatedValidationSupportAdapter(FhirContextProvider.instance());
        String valueSetsLocation = PreferenceService.getString("value_set_location");
        if (valueSetsLocation != null && !valueSetsLocation.isEmpty()){
            addCustomTerminologyItem(valueSetsLocation);
        }
        String codeSystemsLocation = PreferenceService.getString("code_system_location");
        if (codeSystemsLocation != null && !codeSystemsLocation.isEmpty()){
            addCustomTerminologyItem(codeSystemsLocation);
        }
        provideStructureDefinitions();
        return raisedExceptionCounter;
    }

    /**
     * Set the validation support used (default for fhir resources, prePopulated for gazelle loaded resources)
     * @param validator the passed validator (unused currently)
     * @return a validation support chain
     */

    public synchronized static ValidationSupportChainAdapter getValidationSupportChain(net.ihe.gazelle.fhir.validator.common.business.FhirResourceValidatorDescription validator) {
        return new ValidationSupportChainAdapter(defaultProfileValidationSupportAdapter, prePopulatedValidationSupportAdapter, inMemoryTerminologyServerValidationSupport);
    }

    /**
     * Add custom terminology item to prePopulatedValidationSupportAdapter from file location
     * @param location location of the terminology item
     */
    private static void addCustomTerminologyItem(String location) {
        LOG.info("Loading codes from disk at: {}", location);
        File directory = new File(location);
        if (directory.exists() && directory.isDirectory()) {
            File[] valueSetFiles = directory.listFiles(filter);
            if (valueSetFiles != null) {
                for (File file : valueSetFiles) {
                    try {
                        FileInputStream fis = new FileInputStream(file);
                        loadCustomTerminologyItem(fis);
                    } catch (FileNotFoundException e) {
                        LOG.warn(e.getMessage());
                        raisedExceptionCounter++;
                    } catch (UnexpectedResourceException e) {
                        LOG.warn("Unknown terminology item in file : {}", file.getAbsolutePath());
                        raisedExceptionCounter++;
                    }
                }
            }
        }
    }

    /**
     * Load custom terminology item to prePopulatedValidationSupportAdapter from file content
     * @param content input stream
     */
    private static void loadCustomTerminologyItem(InputStream content) throws UnexpectedResourceException {
        if (content != null) {
            try (InputStreamReader reader = new InputStreamReader(content, StandardCharsets.UTF_8)) {
                IParser parser = FhirParserProvider.getXmlParser();
                IBaseResource resource = parser.parseResource(reader);
                if (resource instanceof Bundle) {
                    Bundle bundle = (Bundle) resource;
                    Iterator iterator = bundle.getEntry().iterator();
                    while (iterator.hasNext()) {
                        Bundle.BundleEntryComponent next = (Bundle.BundleEntryComponent) iterator.next();
                        IBaseResource currentResource = next.getResource();
                        processCustomTerminologyItem(currentResource);
                    }
                } else {
                    processCustomTerminologyItem(resource);
                }
            } catch (IOException e) {
                LOG.error("Error closing code system reader !", e);
            }
        }
    }

    /**
     * Process custom terminology item by adding it to prePopulatedValidationSupportAdapter from resource
     * @param currentResource resource input
     */
    private static void processCustomTerminologyItem(IBaseResource currentResource) throws UnexpectedResourceException {
        if (currentResource instanceof CodeSystem) {
            prePopulatedValidationSupportAdapter.addCodeSystem(currentResource);
        } else if (currentResource instanceof ValueSet) {
            prePopulatedValidationSupportAdapter.addValueSet((ValueSet) currentResource);
        } else {
            throw new UnexpectedResourceException("This resource is neither a code system or a value set but referenced in the file system.");
        }
    }

    /**
     * load structure definitions
     */
    private static void provideStructureDefinitions() {
        FhirResourceValidatorDescriptionQuery query = new FhirResourceValidatorDescriptionQuery();
        query.customStructureDefinition().isNotNull();
        query.useIGFhirService().eq(false);
        query.weight().order(false);
        query.available().eq(true);
        List<FhirResourceValidatorDescription> validators = query.getList();
        if (!validators.isEmpty()) {
            HapiStructureDefLoaderAdapter adapter = new HapiStructureDefLoaderAdapter(defaultProfileValidationSupportAdapter,prePopulatedValidationSupportAdapter);
            for (FhirResourceValidatorDescription validatorDescription : validators) {
                raisedExceptionCounter += adapter.loadStructureDefFromValidatorDescription(validatorDescription);
            }
        }
    }
}
