package net.ihe.gazelle.fhir.validator.common.peripherals.mapper;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

public class MapperAdaptor  {
    MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();

    public <S, D> D map(S source, Class<D> destinationClass) {
        return this.mapperFactory.getMapperFacade().map(source, destinationClass);
    }
}
