package net.ihe.gazelle.fhir.validator.common.peripherals.hapi;

import ca.uhn.fhir.context.FhirContext;
import net.ihe.gazelle.fhir.context.FhirContextProvider;
import net.ihe.gazelle.fhir.validator.common.adapter.FhirResourceValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.business.Exception.FhirValidatorException;
import net.ihe.gazelle.fhir.validator.common.peripherals.hl7Fhir.DefaultProfileValidationSupportAdapter;
import net.ihe.gazelle.fhir.validator.common.peripherals.hl7Fhir.PrePopulatedValidationSupportAdapter;
import org.hl7.fhir.r4.conformance.ProfileUtilities;
import org.hl7.fhir.r4.context.IWorkerContext;
import org.hl7.fhir.r4.context.SimpleWorkerContext;
import org.hl7.fhir.r4.hapi.ctx.HapiWorkerContext;
import org.hl7.fhir.r4.model.StructureDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * Adapter for Hapi library :
 * Loading SD
 */
public class HapiStructureDefLoaderAdapter {
    private static final String HL7_PREFIX = "http://hl7.org";
    private static final Logger LOG = LoggerFactory.getLogger(HapiStructureDefLoaderAdapter.class);
    private HapiAdapterDAO dao = new HapiAdapterDAO();
    private DefaultProfileValidationSupportAdapter defaultProfileValidationSupportAdapter;
    private PrePopulatedValidationSupportAdapter prePopulatedValidationSupportAdapter;
    private int raisedExceptionsCounter;

    /**
     * Private default constructor for the class
     */
    private HapiStructureDefLoaderAdapter() {
        //Empty
    }

    /**
     * Constructor to initialize parameters
     *
     * @param defaultProfileValidationSupportAdapter validation support for default profile
     * @param prePopulatedValidationSupportAdapter   validation support for custom profile where structure def will be loaded
     */
    public HapiStructureDefLoaderAdapter(DefaultProfileValidationSupportAdapter defaultProfileValidationSupportAdapter,
                                         PrePopulatedValidationSupportAdapter prePopulatedValidationSupportAdapter) {
        this.prePopulatedValidationSupportAdapter = prePopulatedValidationSupportAdapter;
        this.defaultProfileValidationSupportAdapter = defaultProfileValidationSupportAdapter;
    }

    /**
     * Load the structure definition from validator description
     *
     * @param validatorDescription to get the struct def from
     * @return raised exceptions counter
     */
    public int loadStructureDefFromValidatorDescription(FhirResourceValidatorDescription validatorDescription) {
        raisedExceptionsCounter = 0;
        if (validatorDescription.getContextPath() != null && !validatorDescription.getContextPath().isEmpty()) {
            try (Stream<Path> files = Files.list(Paths.get(validatorDescription.getContextPath()))) {
                files.filter(p -> !p.getFileName().toString().endsWith(".snapshot.xml") && p.toString().endsWith(".xml"))
                        .sorted()
                        .forEach(file -> {
                            try {
                                loadStructureDefFromFile(file.toFile());
                            } catch (FhirValidatorException e) {
                                LOG.error(String.format("Error computing Snapshot for resource file : %s ----> %s", file, e.getCause().getMessage()));
                                raisedExceptionsCounter++;
                            }
                        });
            } catch (IOException e) {
                LOG.error(e.getMessage());
                raisedExceptionsCounter++;
            }
        }
        //Loads the SD ONLY IF not already loaded in context
        if (validatorDescription.getCustomStructureDefinition() != null &&
                (validatorDescription.getContextPath() == null
                        || validatorDescription.getContextPath().isEmpty()
                        || !validatorDescription.getCustomStructureDefinition().contains(validatorDescription.getContextPath()))) {
            File structFile = new File(validatorDescription.getCustomStructureDefinition());
            try {
                loadStructureDefFromFile(structFile);
            } catch (FhirValidatorException e) {
                LOG.error(String.format("Error computing Snapshot for resource file : %s ----> %s", structFile.getPath(), e.getCause().getMessage()));
                raisedExceptionsCounter++;
            }
        }
        return raisedExceptionsCounter;
    }

    /**
     * Load the structure definition from files
     *
     * @param structFile structure definition file
     */
    private void loadStructureDefFromFile(File structFile) throws FhirValidatorException {
        String snapshotPath = getSnapshotFilePath(structFile.getPath());
        File snapFilePath = new File(snapshotPath);
        StructureDefinition structureDefinition;
        if (!snapFilePath.exists() || snapFilePath.lastModified() < structFile.lastModified()) {
            LOG.info(String.format("Structure definition is newer than snapshot for profile [%s] , compute it again",
                    structFile.getPath()));
            structureDefinition = dao.buildStructureDefinitionFromFile(structFile.getPath());
            generateSnapshot(structureDefinition);
            dao.saveSnapshotOnDisk(structureDefinition, snapshotPath);
        } else {
            LOG.info("Snapshot Structure definition is already the newest, use it");
            structureDefinition = dao.buildStructureDefinitionFromFile(snapFilePath.getPath());
        }
        prePopulatedValidationSupportAdapter.addStructureDefinition(structureDefinition);
    }

    /**
     * Generate snapshot of structure definition
     *
     * @param structureDefinition the structure definition to export as a snapshot
     */
    private void generateSnapshot(StructureDefinition structureDefinition) throws FhirValidatorException {
        String url = structureDefinition.getUrl();
        StructureDefinition baseStructureDefinition = null;
        final String baseDefinition = structureDefinition.getBaseDefinition();
        if (baseDefinition != null && !baseDefinition.isEmpty()) {
            if (baseDefinition.startsWith(HL7_PREFIX)) {
                baseStructureDefinition = (StructureDefinition) defaultProfileValidationSupportAdapter.fetchStructureDefinition(baseDefinition);
            } else {
                baseStructureDefinition = (StructureDefinition) prePopulatedValidationSupportAdapter.fetchStructureDefinition(baseDefinition);
            }
            if (baseStructureDefinition == null) {
                LOG.error("Cannot get base definition for snapshot generation: " + baseDefinition);
            }
        }
        try {
            IWorkerContext workerContext = new HapiWorkerContext(FhirContextProvider.instance(), defaultProfileValidationSupportAdapter);

            ProfileUtilities utilities = new ProfileUtilities(workerContext, new ArrayList<>(), new SimpleWorkerContext());
            utilities.generateSnapshot(baseStructureDefinition, structureDefinition, url, structureDefinition.getName(),
                    structureDefinition.getName());
        } catch (Exception e) {
            throw new FhirValidatorException("Error generating snapshot", e);
        }
    }


    /**
     * get corresponding snapshot file path
     *
     * @param path the normal path
     * @return the snapshot path
     */
    private String getSnapshotFilePath(String path) {
        if (path != null && !path.isEmpty()) {
            return path.replace(".xml", ".snapshot.xml");
        } else {
            return null;
        }
    }

}
