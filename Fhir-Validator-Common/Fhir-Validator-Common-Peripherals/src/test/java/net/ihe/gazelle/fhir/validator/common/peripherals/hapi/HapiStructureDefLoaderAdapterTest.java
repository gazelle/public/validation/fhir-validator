package net.ihe.gazelle.fhir.validator.common.peripherals.hapi;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.context.FhirVersionEnum;
import net.ihe.gazelle.fhir.context.FhirContextProvider;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class HapiStructureDefLoaderAdapterTest {

    /**
     * check constructor set context on current version
     */
    @Test
    public void testConstructorVersion() {
        FhirContext context =  FhirContextProvider.instance();
        assertTrue(context.getVersion().getVersion().equals(FhirVersionEnum.R4));
    }
}
