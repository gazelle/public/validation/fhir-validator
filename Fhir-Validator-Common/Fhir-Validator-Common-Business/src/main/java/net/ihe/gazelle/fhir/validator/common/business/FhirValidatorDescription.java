package net.ihe.gazelle.fhir.validator.common.business;

import ca.uhn.fhir.rest.api.EncodingEnum;
import net.ihe.gazelle.validator.validation.model.ValidatorDescription;

import java.io.Serializable;

/**
 * <p>FhirValidatorDescription class.</p>
 *
 * @author abe
 * @version 1.0: 03/01/18
 */

public class FhirValidatorDescription implements ValidatorDescription, Serializable {

    private static final String COPY_SUFFIX = ".copy";

    private String oid;

    private String name;

    private String descriminator;

    private String profile;

    // encoding format will be detected by the validator at execution time
    private EncodingEnum format;

    private boolean available;




    public FhirValidatorDescription() {
        //Empty constructor
    }

    public FhirValidatorDescription(FhirValidatorDescription original){
        this.oid = original.getOid();
        this.name = original.getName() + COPY_SUFFIX;
        this.descriminator = original.getDescriminator();
        this.profile = original.getProfile();
        this.available = false;
    }

    /**
     * This constructor is used for unit testing
     * @param name
     * @param oid
     * @param descriminator
     * @param profile
     */
    public FhirValidatorDescription(String name, String oid, String descriminator, String profile) {
        this.name = name;
        this.oid = oid;
        this.descriminator = descriminator;
        this.profile = profile;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescriminator(String descriminator) {
        this.descriminator = descriminator;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public EncodingEnum getFormat() {
        return format;
    }

    public void setFormat(EncodingEnum format) {
        this.format = format;
    }

    @Override
    public String getOid() {
        return this.oid;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getDescriminator() {
        return this.descriminator;
    }

    // we are not dealing with soap messages, those methods are not used
    @Override
    public String getRootElement() {
        return null;
    }

    @Override
    public String getNamespaceURI() {
        return null;
    }

    @Override
    public boolean extractPartToValidate() {
        return false;
    }


    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }


}
