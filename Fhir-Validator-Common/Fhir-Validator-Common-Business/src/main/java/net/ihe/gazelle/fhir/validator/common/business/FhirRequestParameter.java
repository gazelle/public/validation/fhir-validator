package net.ihe.gazelle.fhir.validator.common.business;

import java.io.Serializable;
import java.util.List;

/**
 * <p>FhirRequestParameter class.</p>
 *
 * @author abe
 * @version 1.0: 27/08/18
 */

public class FhirRequestParameter implements IFhirRequestParameter, Serializable {

    private String name;

    private boolean required;

    private String regex;

    private FhirParameterPrefix prefix;

    private ParameterType type;

    private FhirURLValidatorDescription validatorDescription;

    private List<ValueSetForParameter> allowedListsOfCodes;

    public FhirRequestParameter(){
        this.required = false;
        this.type = ParameterType.STRING;
    }

    public FhirRequestParameter(FhirURLValidatorDescription inValidatorDescription){
        this.validatorDescription = inValidatorDescription;
        this.required = false;
        this.type = ParameterType.STRING;
    }

    public FhirRequestParameter(String inName, ParameterType inType, boolean isRequired, String inRegex) {
        this.name = inName;
        this.type = inType;
        this.required = isRequired;
        this.regex = inRegex;
    }


    public FhirParameterPrefix getPrefix() {
        return prefix;
    }

    public void setPrefix(FhirParameterPrefix prefix) {
        this.prefix = prefix;
    }

    @Override
    public ParameterType getType() {
        return type;
    }

    public void setType(ParameterType type) {
        this.type = type;
    }


    public FhirURLValidatorDescription getValidatorDescription() {
        return validatorDescription;
    }

    public void setValidatorDescription(FhirURLValidatorDescription validatorDescription) {
        this.validatorDescription = validatorDescription;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }


    @Override
    public String getRegex() {
        return regex;
    }

    @Override
    public String getNameWithPrefix() {
        String prefixedName = this.name;
        if (prefix != null){
            prefixedName = prefixedName.concat(":").concat(prefix.getPrefixValue());
        }
        return prefixedName;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FhirRequestParameter)) {
            return false;
        }

        FhirRequestParameter that = (FhirRequestParameter) o;

        if (required != that.required) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        return regex != null ? regex.equals(that.regex) : that.regex == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (required ? 1 : 0);
        result = 31 * result + (regex != null ? regex.hashCode() : 0);
        return result;
    }

    public List<ValueSetForParameter> getAllowedListsOfCodes() {
        return allowedListsOfCodes;
    }

    public void setAllowedListsOfCodes(List<ValueSetForParameter> allowedListsOfCodes) {
        this.allowedListsOfCodes = allowedListsOfCodes;
    }
}
