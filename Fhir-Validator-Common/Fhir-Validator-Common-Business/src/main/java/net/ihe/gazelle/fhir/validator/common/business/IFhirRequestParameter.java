package net.ihe.gazelle.fhir.validator.common.business;

/**
 * <p>IFhirRequestParameter interface.</p>
 *
 * @author abe
 * @version 1.0: 28/08/18
 */
public interface IFhirRequestParameter {

    String getName();

    ParameterType getType();

    String getRegex();

    String getNameWithPrefix();
}
