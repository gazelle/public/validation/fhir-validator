package net.ihe.gazelle.fhir.validator.common.business;

/**
 * <p>FhirParameterPrefix enum.</p>
 *
 * @author abe
 * @version 1.0: 27/08/18
 */
public enum FhirParameterPrefix {

    // FIXME see https://www.hl7.org/fhir/search.html#prefix for values
    EQ("eq"),
    NE("ne"),
    GT("gt"),
    LT("lt"),
    GE("ge"),
    LE("le"),
    SA("sa"),
    EB("eb"),
    AP("ap");

    FhirParameterPrefix(String inPrefixValue) {
        this.prefixValue = inPrefixValue;
    }

    String prefixValue;

    public static FhirParameterPrefix extractPrefixFromValue(String value) {
        // prefix are two characters long
        String twoFirstChar = value.substring(0, 2);
        for (FhirParameterPrefix prefix: values()){
            if (prefix.getPrefixValue().equals(twoFirstChar)){
                return prefix;
            }
        }
        return null;
    }

    public String getPrefixValue() {
        return prefixValue;
    }
}
