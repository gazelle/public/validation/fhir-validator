package net.ihe.gazelle.fhir.validator.common.business.Exception;

public class UnexpectedResourceException extends Exception {

    public UnexpectedResourceException(Exception cause){
        super(cause);
    }

    public UnexpectedResourceException(String message){
        super(message);
    }

    public UnexpectedResourceException(String message, Exception cause){
        super(message, cause);
    }

    public UnexpectedResourceException() {
        super();
    }

}
