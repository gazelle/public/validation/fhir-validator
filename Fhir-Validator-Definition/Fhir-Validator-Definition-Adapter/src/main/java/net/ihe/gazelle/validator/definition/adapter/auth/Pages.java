package net.ihe.gazelle.validator.definition.adapter.auth;

import net.ihe.gazelle.pages.Authorization;
import net.ihe.gazelle.pages.Page;

public enum Pages implements Page {

    HOME("/home.xhtml", Constants.FA_WRENCH, "gazelle.evs.client.home",
         Authorizations.ALL),

    CONFIGURE("/administration/configure.xhtml", Constants.FA_WRENCH,
            "gazelle.fhirvalidator.configure", Authorizations.ADMIN),

    STATISTICS("/administration/usageStatistics.xhtml", Constants.FA_WRENCH,
            "gazelle.fhirvalidator.statistics", Authorizations.ADMIN),

    VALIDATOR_LIST("/validators/list.xhtml", Constants.FA_WRENCH,
            "gazelle.fhirvalidator.validatorlist", Authorizations.ALL),

    DISPLAY_URL_VALIDATOR("/validators/displayURLValidator.xhtml", Constants.FA_WRENCH,
            "gazelle.fhirvalidator.displayURLvalidator", Authorizations.ALL),

    EDIT_URL_VALIDATOR("/validators/editURLValidator.xhtml", Constants.FA_WRENCH,
            "gazelle.fhirvalidator.editURLvalidator", Authorizations.ADMIN),

    DISPLAY_RESOURCE_VALIDATOR("/validators/displayResourceValidator.xhtml", Constants.FA_WRENCH,
                           "gazelle.fhirvalidator.displayresourcevalidator", Authorizations.ALL),

    EDIT_RESOURCE_VALIDATOR("/validators/editResourceValidator.xhtml", Constants.FA_WRENCH,
            "gazelle.fhirvalidator.editresourcevalidator", Authorizations.ADMIN);

    private String link;
    private final String icon;
    private String label;
    private final Authorization[] authorizations;

    Pages(final String link, final String icon, final String label, final Authorization... authorizations) {
        this.link = link;
        this.authorizations = authorizations.clone();
        this.label = label;
        this.icon = icon;
    }

    @Override
    public String getId() {
        return this.name();
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public String getLink() {
        return this.link;
    }

    @Override
    public String getIcon() {
        return this.icon;
    }

    @Override
    public Authorization[] getAuthorizations() {
        if(this.authorizations != null) {
            return this.authorizations.clone();
        }
        else {
            return null;
        }
    }

    @Override
    public String getMenuLink() {
        return this.link.replace(".xhtml", ".seam");
    }

    private static class Constants {
        public static final String FA_WRENCH = "fa-wrench";
    }
}
