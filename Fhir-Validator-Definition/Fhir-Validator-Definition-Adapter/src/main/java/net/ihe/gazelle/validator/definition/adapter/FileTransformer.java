package net.ihe.gazelle.validator.definition.adapter;

import net.ihe.gazelle.preferences.PreferenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * <p>FileTransformer class.</p>
 *
 * @author abe
 * @version 1.0: 26/04/18
 */

public class FileTransformer {

    private static final Logger LOG = LoggerFactory.getLogger(FileTransformer.class);

    private static Transformer transformer;

    static {
        initializeTransformer();
    }

    private static void initializeTransformer() {
        final TransformerFactory tFactory = TransformerFactory.newInstance();
        String stylesheetLocation = PreferenceService.getString("structure_definition_stylesheet_location");
        if (stylesheetLocation != null) {
            StreamSource xslSource = new StreamSource(stylesheetLocation);
            transformer = null;
            try {
                transformer = tFactory.newTransformer(xslSource);
                transformer.setOutputProperty(OutputKeys.ENCODING, StandardCharsets.UTF_8.name());
                transformer.setOutputProperty(OutputKeys.METHOD, "html");
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            } catch (TransformerConfigurationException e1) {
                LOG.warn(e1.getMessage());
            }
        }
    }

    public static String convertXmlStructureDefinitionToHtml(String filePath) {
        String transformedFile = null;
        final ByteArrayOutputStream resultAsByteArray = new ByteArrayOutputStream();
        try (FileReader fileReader = new FileReader(filePath)) {
            StreamSource fileSource = new StreamSource(fileReader);
            final StreamResult streamResult = new StreamResult(resultAsByteArray);
            transformer.transform(fileSource, streamResult);
            transformedFile = resultAsByteArray.toString(StandardCharsets.UTF_8.name());
        } catch (FileNotFoundException e) {
            LOG.warn(e.getMessage());
            return "Snapshot has not been computed";
        } catch (TransformerException | IOException e) {
            LOG.warn(e.getMessage());
        }
        if (transformedFile != null) {
            return transformedFile;
        } else {
            return "No stylesheet defined to display this content";
        }
    }
}
