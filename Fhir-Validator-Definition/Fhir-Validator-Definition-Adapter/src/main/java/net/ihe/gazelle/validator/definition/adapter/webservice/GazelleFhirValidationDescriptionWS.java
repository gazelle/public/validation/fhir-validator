/*
 * Copyright 2012 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.validator.definition.adapter.webservice;


import net.ihe.gazelle.fhir.validator.common.adapter.FhirValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.adapter.dao.FhirValidatorDescriptionDAO;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.ext.Provider;
import java.io.Serializable;

/**
 * web service to act as a ValidatorDescription provider service
 */
@Provider
@Path(value="/fhirValidatorDescription")
@Named("fhirValidatorDescriptionWebservice")
public class GazelleFhirValidationDescriptionWS implements Serializable {

    private FhirValidatorDescriptionDAO dao;

    @Inject
    public void setFhirValidatorDescriptionDAO(FhirValidatorDescriptionDAO dao){
        this.dao = dao;
    }


    /**
     * get ValidatorDescription
     * @param id the validator OID
     * @return ValidatorDescription
     */
    @GET
    public FhirValidatorDescription getValidatorByOidOrName(@QueryParam("OID")String id) {
        return (FhirValidatorDescription)dao.getValidatorByOidOrName(id);
    }

    /**
     * get ValidatorDescription from database ID.
     * @param validatorId the database ID
     * @return ValidatorDescription
     */
    @Deprecated
    // TODO: 27/11/2019  database id should not be exposed. Need to have OID in url param
    public FhirValidatorDescription getValidatorByDatabaseID(Class entityClass, Integer validatorId) {
        return dao.findWithId(entityClass, validatorId);
    }

    /**
     * save the validator
     * @param fhirValidatorDescription the validator description to save
     * @return the saved validator
     */
    @POST
    public FhirValidatorDescription saveFhirValidatorDescription(FhirValidatorDescription fhirValidatorDescription) {
        return dao.saveFhirValidatorDescription(fhirValidatorDescription);
    }


}