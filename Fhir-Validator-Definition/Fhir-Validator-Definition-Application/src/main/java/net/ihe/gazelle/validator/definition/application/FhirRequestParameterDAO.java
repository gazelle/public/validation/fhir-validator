package net.ihe.gazelle.validator.definition.application;


import net.ihe.gazelle.fhir.validator.common.business.FhirRequestParameter;


public interface FhirRequestParameterDAO {

    void deleteFhirRequestParameter(String ParameterId);
}
