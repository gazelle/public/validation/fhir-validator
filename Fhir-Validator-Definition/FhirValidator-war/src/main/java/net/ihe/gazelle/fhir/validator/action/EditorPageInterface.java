package net.ihe.gazelle.fhir.validator.action;

public interface EditorPageInterface {

    String getDisplayPage();
}
