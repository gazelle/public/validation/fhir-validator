package net.ihe.gazelle.fhir.validator.action;

import net.ihe.gazelle.fhir.validator.common.adapter.FhirResourceValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.adapter.FhirValidatorDescription;
import net.ihe.gazelle.preferences.GenericConfigurationManager;
import net.ihe.gazelle.validator.definition.adapter.webservice.GazelleFhirValidationDescriptionWS;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Map;

/**
 * <p>FhirvalidatorDescriptionManager class.</p>
 *
 * @author abe
 * @version 1.0: 10/04/18
 */

public abstract class FhirvalidatorDescriptionManager<T extends FhirValidatorDescription> implements Serializable, EditorPageInterface {

    protected GenericConfigurationManager genericConfigurationManager;
    protected GazelleFhirValidationDescriptionWS fhirValidatorDescriptionWebService;
    private T validatorDescription;
    protected String paramId;

    public T getValidatorDescription() {
        return validatorDescription;
    }

    public void setValidatorDescription(FhirValidatorDescription validatorDescription) {
        this.validatorDescription = (T) validatorDescription;
    }

    public void getValidatorDescriptionFromUrl() {
        String idString = getIdParameterFromUrl();
        if (idString != null) {
            validatorDescription = getValidatorForIdParameter(idString);
        } else {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,"No id parameter found",
                    "The id parameter is expected");
            facesContext.addMessage(null, facesMessage);
        }
    }

    protected T getValidatorForIdParameter(String idValue) {
        try {
            T validator = (T) fhirValidatorDescriptionWebService.getValidatorByOidOrName(idValue);
            if (validator == null) {
                FacesContext facesContext = FacesContext.getCurrentInstance();
                FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Matching id",
                        "No FHIR Validator matches the given id");
                facesContext.addMessage(null, facesMessage);
            }
            return validator;
        } catch (NumberFormatException e) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error interpreting id",
                    "id is not a valid value");
            facesContext.addMessage(null, facesMessage);
        }
        return null;
    }

    protected abstract Class<T> getEntityClass();

    protected String getIdParameterFromUrl() {
       return getParameterFromUrl("id");
    }

    protected String getParameterFromUrl(String paramName){
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        return params.get(paramName);
    }

    public String backTolist() {
        return genericConfigurationManager.getApplicationUrl() + "/validators/list.seam";
    }

    public String editValidator(FhirValidatorDescription description) {
        if (description != null){
            if (description instanceof FhirResourceValidatorDescription) {
                return genericConfigurationManager.getApplicationUrl() + "/validators/editResourceValidator.seam?id=" + description.getOid();
            } else {
                return genericConfigurationManager.getApplicationUrl() + "/validators/editURLValidator.seam?id=" + description.getOid();
            }
        } else {
            return null;
        }
    }

    public String save(){
        FhirValidatorDescription existingEntity = null;
        if ("new".equals(paramId) || "copy".equals(paramId) || !getValidatorDescription().getOid().equals(paramId)){
            existingEntity = fhirValidatorDescriptionWebService.getValidatorByOidOrName(getValidatorDescription().getOid());
        }

        FacesMessage facesMessage;
        if(existingEntity == null) {
            FhirValidatorDescription savedEntity = fhirValidatorDescriptionWebService.saveFhirValidatorDescription(getValidatorDescription());
            setValidatorDescription(getValidatorDescription().getClass().cast(savedEntity));

            facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Changes have been saved",
                    "Changes have been saved");
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.addMessage(null, facesMessage);
            String params = "?id=" + savedEntity.getOid() + "&faces-redirect=true";
            return getDisplayPage() + params;
        }
        else {
            facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "OID already used",
                    "Changes have not been saved");
        }
        FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.addMessage(null, facesMessage);
        return null;
    }

    public String displayValidator(FhirValidatorDescription description) {
        if (description != null){
            if (description instanceof FhirResourceValidatorDescription) {
                return genericConfigurationManager.getApplicationUrl() + "/validators/displayResourceValidator.seam?id=" + description.getOid();
            } else {
                return genericConfigurationManager.getApplicationUrl() + "/validators/displayURLValidator.seam?id=" + description.getOid();
            }
        } else {
            return null;
        }

    }
}
