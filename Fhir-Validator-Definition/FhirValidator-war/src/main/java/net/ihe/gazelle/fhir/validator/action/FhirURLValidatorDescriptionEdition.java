package net.ihe.gazelle.fhir.validator.action;

import net.ihe.gazelle.fhir.validator.common.adapter.*;

import net.ihe.gazelle.fhir.validator.common.business.FhirParameterPrefix;
import net.ihe.gazelle.fhir.validator.common.business.ParameterType;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.preferences.GenericConfigurationManager;
import net.ihe.gazelle.validator.definition.adapter.auth.Pages;
import net.ihe.gazelle.validator.definition.adapter.webservice.GazelleFhirValidationDescriptionWS;
import net.ihe.gazelle.validator.definition.adapter.webservice.GazelleFhirValidationParameterWS;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * <p>FhirValidatorDescriptionDisplay class.</p>
 *
 * @author abe
 * @version 1.0: 09/04/18
 */
@ManagedBean(name = "fhirURLValidatorDescriptionEdition")
@ViewScoped
public class FhirURLValidatorDescriptionEdition extends FhirvalidatorDescriptionManager<FhirURLValidatorDescription> {

    private FhirRequestParameter selectedParameter;

    private GazelleFhirValidationParameterWS gazelleFhirValidationParameterWS;

    @Inject
    public void setGazelleFhirValidationParameterWS(GazelleFhirValidationParameterWS gazelleFhirValidationParameterWS){
        this.gazelleFhirValidationParameterWS = gazelleFhirValidationParameterWS;
    }


    @Inject
    public void setFhirValidatorDescriptionTransactionManager(GazelleFhirValidationDescriptionWS fhirValidatorDescriptionWebService){
        this.fhirValidatorDescriptionWebService = fhirValidatorDescriptionWebService;
    }

    @Inject
    public void setApplicationConfigurationManager(GenericConfigurationManager genericConfigurationManager){
        this.genericConfigurationManager = genericConfigurationManager;
    }

    @PostConstruct
    public void initialize(){
        String id = getIdParameterFromUrl();
        if ("new".equals(id)){
            setValidatorDescription(new FhirURLValidatorDescription());
            paramId = "new";
        } else {
            String copy = getParameterFromUrl("copy");
            if ("true".equals(copy)){
                paramId = "copy";
                FhirURLValidatorDescription validatorDescription = getValidatorForIdParameter(id);
                if (validatorDescription != null) {
                    setValidatorDescription(new FhirURLValidatorDescription(validatorDescription));
                } else {
                    FacesContext facesContext = FacesContext.getCurrentInstance();
                    FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error retrieving entry",
                            "No entry matches id " + id);
                    facesContext.addMessage(null, facesMessage);
                }
            } else {
                paramId = id;
                setValidatorDescription(getValidatorForIdParameter(id));
            }
        }
    }

    public List<FhirParameterPrefix> getPrefixes(){
        List<FhirParameterPrefix> prefixes = Arrays.asList(FhirParameterPrefix.values());
        Collections.sort(prefixes);
        return prefixes;
    }

    public List<ParameterType> getTypes(){
        List<ParameterType> types = Arrays.asList(ParameterType.values());
        Collections.sort(types);
        return types;
    }

    @Override
    protected Class getEntityClass() {
        return FhirURLValidatorDescription.class;
    }

    public FhirRequestParameter getSelectedParameter() {
        return selectedParameter;
    }

    public void setSelectedParameter(FhirRequestParameter selectedParameter) {
        this.selectedParameter = selectedParameter;
    }

    public void addAParameter(){
        this.selectedParameter = new FhirRequestParameter((FhirURLValidatorDescription) getValidatorDescription());
    }

    public void saveParameter(){
        if (this.selectedParameter.getId() == null){
            getValidatorDescription().getRequestParameters().add(this.selectedParameter);
        }
        save();
        this.selectedParameter = null;
    }

    public void deleteParameter(FhirRequestParameter parameter){
        getValidatorDescription().getRequestParameters().remove(parameter);
        save();
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        FhirRequestParameter toDelete = entityManager.find(FhirRequestParameter.class, parameter.getId());
        gazelleFhirValidationParameterWS.deleteParameter(toDelete.getId().toString());
    }

    @Override
    public String getDisplayPage() {
        return Pages.DISPLAY_URL_VALIDATOR.getLink();
    }

}
