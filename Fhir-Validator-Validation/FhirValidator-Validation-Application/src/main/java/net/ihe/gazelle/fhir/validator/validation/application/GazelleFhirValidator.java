package net.ihe.gazelle.fhir.validator.validation.application;

import ca.uhn.fhir.rest.api.EncodingEnum;
import ca.uhn.fhir.validation.*;
import com.google.gson.Gson;
import net.ihe.gazelle.fhir.validator.FhirURLValidator;
import net.ihe.gazelle.fhir.validator.common.business.Exception.FhirValidatorException;
import net.ihe.gazelle.fhir.validator.common.business.FhirResourceValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.business.FhirURLValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.business.FhirValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.business.ValidatorConstants;
import net.ihe.gazelle.fhir.validator.common.peripherals.hapi.HapiAdapterDAO;
import net.ihe.gazelle.fhir.validator.common.peripherals.mapper.MapperAdaptor;
import net.ihe.gazelle.fhir.validator.validation.application.operationoutcome.OperationOutcomeValidator;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.*;
import net.ihe.gazelle.validator.validation.exception.GazelleValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * <p>GazelleFhirValidator class.</p>
 *
 * @author aberge
 * @version 1.0: 16/11/17
 */

public class GazelleFhirValidator {

    private static final Logger LOG = LoggerFactory.getLogger(GazelleFhirValidator.class);
    private Integer countWarningMessage = 0;
    private Integer countErrorMessage = 0;
    private String validationStatus;
    private FhirValidatorDescription validatorDescription;
    private MapperAdaptor mapper = new MapperAdaptor();
    private IGFhirServerClient igFhirServerClient;

    public GazelleFhirValidator(FhirValidatorDescription inValidatorName) {
        this.validatorDescription = inValidatorName;
        this.validationStatus = null;
    }

    /**
     * Setter for the property.
     *
     * @param igFhirServerClient value to set to the property.
     */
    public void setIgFhirServerClient(IGFhirServerClient igFhirServerClient) {
        this.igFhirServerClient = igFhirServerClient;
    }

    public String getStatus() {
        return this.validationStatus;
    }

    public String validateFhirMessage(String messageAsString) {
        DetailedResult result;
        try {
            if (validatorDescription instanceof FhirURLValidatorDescription) {
                result = validateFhirRequestAsUrl(messageAsString);
            } else if (validatorDescription instanceof FhirResourceValidatorDescription && !((FhirResourceValidatorDescription) validatorDescription).isUseIGFhirService()) {
                detectEncodingFormat(messageAsString);
                result = validateFhirResource(messageAsString);
            } else {
                detectEncodingFormat(messageAsString);
                result = sendToIGFHIRServer(messageAsString);
            }
        } catch (FhirValidatorException e) {
            LOG.error(e.getMessage());
            result = ReportCreator.buildReportForAbortedValidation(validatorDescription,new IOException(e.getMessage()));
        }
        return extractStatusAndConvertToString(result);
    }

    /**
     * Check that a message is well formed dependeing of its format.
     *
     * @param messageToValidate literal content of the message to validate
     * @param result            validation report to fill.
     * @return true if the message is well formed, false otherwise.
     */
    private boolean isMessageWellFormed(String messageToValidate, DetailedResult result) {
        if (EncodingEnum.XML.equals(validatorDescription.getFormat())) {
            checkCorrectnessForXMLMessage(messageToValidate, result);
        } else {
            isJSONValid(messageToValidate, result);
        }
        return !result.getDocumentWellFormed().getResult().equalsIgnoreCase("failed");
    }

    private void checkCorrectnessForXMLMessage(String messageToValidate, DetailedResult result) {
        DocumentWellFormed documentWellFormedResult;
        XMLValidation xmlValidation = new XMLValidation();
        documentWellFormedResult = xmlValidation.isXMLWellFormed(messageToValidate);
        result.setDocumentWellFormed(documentWellFormedResult);
    }

    /**
     * Send the Message to Validate to the configured IG FHIR Server for validation.
     *
     * @param messageToValidate literal content of the message to validate.
     * @return the validation report.
     */
    private DetailedResult sendToIGFHIRServer(String messageToValidate) {
        DetailedResult result = new DetailedResult();

        if (!isMessageWellFormed(messageToValidate, result)) {
            return result;
        }

        igFhirServerClient.sendMessageForValidation(messageToValidate, mapper.map(validatorDescription, FhirResourceValidatorDescription.class)
                .getStructureDefinitionUrl(), validatorDescription.getFormat(), result);

        boolean passed = ReportCreator.PASSED.equals(result.getDocumentWellFormed().getResult());
        if (passed && result.getMDAValidation() != null) {
            passed = ReportCreator.PASSED.equals(result.getMDAValidation().getResult());
        }
        result.setValidationResultsOverview(ReportCreator.fhirValidationOverview(passed, validatorDescription));
        validationStatus = result.getValidationResultsOverview().getValidationTestResult();
        return result;
    }

    private DetailedResult validateFhirResource(String messageToValidate) {
        DetailedResult result = new DetailedResult();

        if (!isMessageWellFormed(messageToValidate, result)) {
            return result;
        }

        // Run hapi Schema and Schematron validations
        runFhirValidator(messageToValidate, result);


        String profile = validatorDescription.getProfile();

        // Specific profile validation part
        MDAValidation mdaValidation;
        FhirResourceValidatorDescription resourceValidatorDescription = mapper.map(validatorDescription, FhirResourceValidatorDescription.class);
        String customStructureDefinition = resourceValidatorDescription.getCustomStructureDefinition();
        if (customStructureDefinition != null && !customStructureDefinition.isEmpty()) {
            mdaValidation = validateWithCustomStructureDefinition(messageToValidate, resourceValidatorDescription);
        } else if (ValidatorConstants.OPERATION_OUTCOME.equals(profile)) {
            mdaValidation = validateOperationOutcome(messageToValidate, validatorDescription.getFormat());
        } else {
            mdaValidation = null;
        }
        if (mdaValidation != null) {
            result.setMDAValidation(mdaValidation);
        }
        boolean passed = ReportCreator.PASSED.equals(result.getDocumentWellFormed().getResult());
        if (passed) {
            passed = passed && ReportCreator.PASSED.equals(result.getDocumentValidXSD().getResult());
        }
        if (passed && mdaValidation != null) {
            passed = passed && ReportCreator.PASSED.equals(result.getMDAValidation().getResult());
        }
        // Overview validation part
        result.setValidationResultsOverview(ReportCreator.fhirValidationOverview(passed, validatorDescription));
        validationStatus = result.getValidationResultsOverview().getValidationTestResult();
        return result;
    }


    private void runFhirValidator(String messageToValidate, DetailedResult result) {
        FhirValidator validator;
        if (mapper.map(validatorDescription, FhirResourceValidatorDescription.class).isExecuteSchematronValidation()) {
            validator = FhirValidatorProvider.instanceWithSchematron();
        } else {
            validator = FhirValidatorProvider.instanceWithoutSchematron();
        }

        // XSD Validation part
        DocumentValidXSD documentValidXSD = new DocumentValidXSD();

        try {
            ValidationResult validationResult = validator.validateWithResult(messageToValidate);
            for (SingleValidationMessage currentMessage : validationResult.getMessages()) {
                XSDMessage currentXSDMessage = new XSDMessage();
                ResultSeverityEnum severity = currentMessage.getSeverity();
                if (severity.equals(ResultSeverityEnum.ERROR)) {
                    countErrorMessage++;
                } else if (severity.equals(ResultSeverityEnum.WARNING)) {
                    countWarningMessage++;
                } else if (severity.equals(ResultSeverityEnum.FATAL)) {
                    countErrorMessage++;
                }
                // nothing is done with informational data
                currentXSDMessage.setSeverity(severity.getCode());
                currentXSDMessage.setColumnNumber(currentMessage.getLocationCol());
                currentXSDMessage.setLineNumber(currentMessage.getLocationLine());
                StringBuilder messageBuilder = new StringBuilder();
                if (currentMessage.getLocationString() != null) {
                    messageBuilder.append(currentMessage.getLocationString());
                    messageBuilder.append(": ");
                }
                messageBuilder.append(currentMessage.getMessage());
                currentXSDMessage.setMessage(messageBuilder.toString());
                documentValidXSD.getXSDMessage().add(currentXSDMessage);
            }
            documentValidXSD.setNbOfErrors(countErrorMessage.toString());
            documentValidXSD.setNbOfWarnings(countWarningMessage.toString());
            if (validationResult.isSuccessful()) {
                documentValidXSD.setResult(ReportCreator.PASSED);
            } else {
                documentValidXSD.setResult(ReportCreator.FAILED);
            }
        } catch (Exception e) {
            XSDMessage xsdMessage = new XSDMessage();
            xsdMessage.setSeverity(ResultSeverityEnum.FATAL.getCode());
            if (e.getCause() != null) {
                xsdMessage.setMessage(e.getCause() + " - " + e.getMessage());
            } else {
                xsdMessage.setMessage(e.getMessage());
            }
            documentValidXSD.getXSDMessage().add(xsdMessage);
            documentValidXSD.setResult(ReportCreator.FAILED);
        }
        result.setDocumentValidXSD(documentValidXSD);
    }

    private MDAValidation validateWithCustomStructureDefinition(String messageToValidate, FhirResourceValidatorDescription validatorDescription) {
        FhirValidator validator = FhirValidatorProvider.instanceForCustomStructureDefinition(validatorDescription);
        validator.setValidateAgainstStandardSchema(false);
        validator.setValidateAgainstStandardSchematron(false);

        MDAValidation mdaValidationResult = new MDAValidation();
        ValidationResult validationResultWithStrucDef = validator.validateWithResult(messageToValidate,
                new ValidationOptions().addProfile(validatorDescription.getStructureDefinitionUrl()));

        // resetting counters
        countErrorMessage = 0;
        countWarningMessage = 0;
        int countInfoMessage = 0;
        List<SingleValidationMessage> messageList = validationResultWithStrucDef.getMessages();
        for (SingleValidationMessage message : messageList) {
            Notification notification;
            ResultSeverityEnum severity = message.getSeverity();
            switch (severity) {
                case ERROR:
                    notification = new Error();
                    countErrorMessage++;
                    break;
                case FATAL:
                    notification = new Error();
                    countErrorMessage++;
                    break;
                case WARNING:
                    notification = new Warning();
                    countWarningMessage++;
                    break;
                default:
                    notification = new Info();
                    countInfoMessage++;
                    break;
            }
            notification.setLocation(message.getLocationString());
            notification.setDescription(message.getMessage());
            mdaValidationResult.getWarningOrErrorOrNote().add(notification);
        }
        ValidationCounters counters = new ValidationCounters();
        counters.setNrOfChecks(BigInteger.valueOf(messageList.size()));
        counters.setNrOfValidationErrors(BigInteger.valueOf(countErrorMessage));
        counters.setNrOfValidationInfos(BigInteger.valueOf(countInfoMessage));
        counters.setNrOfValidationWarnings(BigInteger.valueOf(countWarningMessage));
        mdaValidationResult.setValidationCounters(counters);
        if (validationResultWithStrucDef.isSuccessful()) {
            mdaValidationResult.setResult("PASSED");
        } else {
            mdaValidationResult.setResult("FAILED");
        }
        return mdaValidationResult;
    }

    private MDAValidation validateOperationOutcome(String messageToValidate, EncodingEnum format) {
        OperationOutcomeValidator validator = new OperationOutcomeValidator(messageToValidate, format);
        return validator.validateOperationOutcome();
    }


    private void isJSONValid(String messageAsString, DetailedResult detailedResult) {
        DocumentWellFormed documentWellFormed = new DocumentWellFormed();
        Gson gson = new Gson();
        try {
            gson.fromJson(messageAsString, Object.class);
            documentWellFormed.setNbOfErrors("0");
            documentWellFormed.setResult(ReportCreator.PASSED);
        } catch (com.google.gson.JsonSyntaxException ex) {
            documentWellFormed.setNbOfErrors("1");
            documentWellFormed.setResult(ReportCreator.FAILED);
            XSDMessage details = new XSDMessage();
            details.setSeverity("error");
            details.setMessage(ex.getMessage());
            documentWellFormed.getXSDMessage().add(details);
        }
        detailedResult.setDocumentWellFormed(documentWellFormed);
    }

    private DetailedResult validateFhirRequestAsUrl(String messageAsString) {
        FhirURLValidatorDescription urlValidatorDescription = (FhirURLValidatorDescription) validatorDescription;
        String decodedUrl = decodeUrlBeforeValidation(messageAsString);
        FhirURLValidator urlValidator = new FhirURLValidator();
        DetailedResult result = new DetailedResult();
        MDAValidation validationResult = urlValidator.validateRequest(decodedUrl, urlValidatorDescription);
        result.setMDAValidation(validationResult);
        boolean status = urlValidator.isValidationPassed();
        result.setValidationResultsOverview(ReportCreator.fhirValidationOverview(status, urlValidatorDescription));
        return result;
    }

    private void detectEncodingFormat(String messageAsString) throws FhirValidatorException {
        if (messageAsString.indexOf("<") < 2 && messageAsString.indexOf("<") >= 0) {
            validatorDescription.setFormat(EncodingEnum.XML);
        } else if (messageAsString.indexOf("{") < 2 && messageAsString.indexOf("{") >= 0) {
            validatorDescription.setFormat(EncodingEnum.JSON);
        } else {
            throw new FhirValidatorException("The format cannot be detected : only XML or JSON can be used");
        }
    }

    private String extractStatusAndConvertToString(DetailedResult detailedResult) {
        if (detailedResult.getValidationResultsOverview() != null) {
            validationStatus = detailedResult.getValidationResultsOverview().getValidationTestResult();
        } else {
            validationStatus = ReportCreator.ABORTED;
        }
        return ReportCreator.getDetailedResultAsString(detailedResult);
    }

    private String decodeUrlBeforeValidation(String encodedUrl) {
        try {
            return URLDecoder.decode(encodedUrl, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            return encodedUrl;
        }
    }
}
