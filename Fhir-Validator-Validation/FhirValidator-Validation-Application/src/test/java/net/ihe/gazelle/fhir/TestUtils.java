package net.ihe.gazelle.fhir;

import net.ihe.gazelle.fhir.validator.common.business.FhirAssertion;
import net.ihe.gazelle.fhir.validator.common.business.FhirResourceValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.business.ValidatorConstants;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * <p>testutils class.</p>
 *
 * @author aberge
 * @version 1.0: 21/11/17
 */

public abstract class TestUtils {

    public static final String ERROR = "Error";
    public static final String WARNING = "Warning";
    public static final String INFO = "Info";

    public static String readFile(String path) throws IOException {
        FileInputStream fis = new FileInputStream(path);
        byte[] encoded = new byte[fis.available()];
        fis.read(encoded);
        return new String(encoded, StandardCharsets.UTF_8);
    }

    protected abstract MDAValidation validate(String message);

    public boolean validateValidFile(String path, FhirAssertion testedAssertion) throws IOException {
        String message = readFile(path);
        MDAValidation result = validate(message);
        if (result != null) {
            Object notification = getNotificationFromReport(result, testedAssertion);
            return (notification != null && notification.getClass().equals(Note.class));
        }
        return false;
    }

    private Object getNotificationFromReport(MDAValidation result, FhirAssertion testedAssertion) {
        List<Object> reports = result.getWarningOrErrorOrNote();
        for (Object report : reports) {
            if (report instanceof Notification) {
                Notification notification = (Notification) report;
                if (testedAssertion.getIdentifier().equals(notification.getIdentifiant())) {
                    return report;
                }
            }
        }
        return null;
    }

    public boolean validateNonValidFile(String path, FhirAssertion testedAssertion, String expectedLevel) throws IOException {
        String message = readFile(path);
        MDAValidation result = validate(message);
        if (result != null) {
            Object notification = getNotificationFromReport(result, testedAssertion);
            if (notification != null) {
                if (expectedLevel.equals(INFO)) {
                    return notification.getClass().equals(Info.class);
                } else if (expectedLevel.equals(WARNING)) {
                    return notification.getClass().equals(Warning.class);
                } else if (expectedLevel.equals(ERROR)) {
                    return notification.getClass().equals(Error.class);
                }
            }
        }
        return false;
    }





    protected FhirResourceValidatorDescription getOperationOutcomXml() {
        return new FhirResourceValidatorDescription("Operation Outcome XML", "1.3.6.1.4.1.12559.11.1.2.1.15.6", ValidatorConstants.IHE_DISCRIMINATOR,
                ValidatorConstants.OPERATION_OUTCOME);
    }

    protected FhirResourceValidatorDescription getPdqmRetrieveResponseXml() {
        return new FhirResourceValidatorDescription("[ITI-78] Retrieve Patient Resource Response XML", "1.3.6.1.4.1.12559.11.1.2.1.15.10", ValidatorConstants
                .IHE_DISCRIMINATOR, ValidatorConstants.PDQM);
    }

    protected FhirResourceValidatorDescription getFhirResourceXml() {
        return new FhirResourceValidatorDescription("FHIR Resource XML (R4)", "1.3.6.1.4.1.12559.11.1.2.1.15.12", ValidatorConstants.FHIR_DISCRIMINATOR, ValidatorConstants

                .FHIR);
    }


}
