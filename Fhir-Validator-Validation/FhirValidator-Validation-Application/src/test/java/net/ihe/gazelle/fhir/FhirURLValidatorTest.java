package net.ihe.gazelle.fhir;

import net.ihe.gazelle.fhir.validator.FhirURLValidator;
import net.ihe.gazelle.fhir.validator.common.business.*;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * <p>FhirURLValidatorTest class.</p>
 *
 * @author abe
 * @version 1.0: 29/08/18
 */

public class FhirURLValidatorTest {

    private FhirURLValidator validator;

    @Before
    public void initializeTest() {
        validator = new FhirURLValidator();
        validator.setValidatorDescription(FhirURLValidatorTestData.getValidatorDescriptionForTest());
    }

    @Test
    public void testSplitFullUrl() {
        String[] parts = validator.splitStringAndGetParts(FhirURLValidatorTestData.URL1_FULL, "\\?");
        Assert.assertEquals(2, parts.length);
        Assert.assertEquals(FhirURLValidatorTestData.URL1_BASE, parts[0]);
        Assert.assertEquals(FhirURLValidatorTestData.URL1_PARAMETERS, parts[1]);
    }

    @Test
    public void testSplitParameters() {
        String[] parts = validator.splitStringAndGetParts(FhirURLValidatorTestData.URL2_PARAMETERS, "&");
        Assert.assertEquals(2, parts.length);
        Assert.assertEquals(FhirURLValidatorTestData.URL2_FIRST_PARAMETER, parts[0]);
        Assert.assertEquals(FhirURLValidatorTestData.URL2_SECOND_PARAMETER, parts[1]);
    }

    @Test
    public void testSplitParameterKeyValue() {
        String[] parts = validator.splitStringAndGetParts(FhirURLValidatorTestData.URL2_SECOND_PARAMETER, "=");
        Assert.assertEquals(2, parts.length);
        Assert.assertEquals(FhirURLValidatorTestData.URL2_SECOND_PARAMETER_KEY, parts[0]);
        Assert.assertEquals(FhirURLValidatorTestData.URL2_SECOND_PARAMETER_VALUE, parts[1]);
    }

    @Test
    public void testSplitParameterKeyModifier() {
        String parts[] = validator.splitStringAndGetParts(FhirURLValidatorTestData.KEY_WITH_CONTAINS_MODIFIER, ":");
        Assert.assertEquals(2, parts.length);
        Assert.assertEquals(FhirURLValidatorTestData.PARAMETER_GIVEN, parts[0]);
        Assert.assertEquals(FhirURLValidatorTestData.PARAMETER_MODIFIER_CONTAINS, parts[1]);
    }

    @Test
    public void testParameterValueFormatDefaultRegex() {
        validator.validateParameterValueFormat(FhirURLValidatorTestData.PARAMETER_GIVEN_VALUE, FhirURLValidatorTestData.PARAM_GIVEN_DESCRIPTION);
        checkRaisedNotification(Note.class);
    }

    @Test
    public void testParameterValueFormatRegex() {
        validator.validateParameterValueFormat(FhirURLValidatorTestData.PARAMETER_DATE_VALUE, FhirURLValidatorTestData.PARAM_DATE_DESCRIPTION);
        checkRaisedNotification(Note.class);
    }

    @Test
    public void koTestParameterValueFormatDefautRegex() {
        validator.validateParameterValueFormat(FhirURLValidatorTestData.PARAMETER_GIVEN_VALUE, FhirURLValidatorTestData.PARAM_NUMBER_DESCRIPTION);
        checkRaisedNotification(Error.class);
    }

    @Test
    public void testParameterFloatNumber() {
        validator.validateParameterValueFormat(FhirURLValidatorTestData.PARAMETER_VALUE_FLOAT, FhirURLValidatorTestData.PARAM_NUMBER_DESCRIPTION);
        checkRaisedNotification(Note.class);
    }

    @Test
    public void testParameterNegativeNumber() {
        validator.validateParameterValueFormat(FhirURLValidatorTestData.PARAMETER_VALUE_NEGATIVE, FhirURLValidatorTestData.PARAM_NUMBER_DESCRIPTION);
        checkRaisedNotification(Note.class);
    }

    @Test
    public void testAllowedParameter() {
        IFhirRequestParameter parameter = validator.getParameterIfAllowed(FhirURLValidatorTestData.PARAMETER_DATE_NAME);
        Assert.assertNotNull(parameter);
        checkRaisedNotification(Note.class);
    }

    @Test
    public void testNotAllowedParameter() {
        IFhirRequestParameter parameter = validator.getParameterIfAllowed(FhirURLValidatorTestData.PARAMETER_NOT_SPECIFIED);
        Assert.assertNull(parameter);
        checkRaisedNotification(Error.class);
    }

    @Test
    public void testAllRequiredParameterPresents() {
        validator.setUsedParameters(FhirURLValidatorTestData.getUsedParameterCompleteList());
        validator.checkAllRequiredParametersArePresent();
        checkRaisedNotification(Note.class);
    }

    @Test
    public void testMissingRequiredParameter() {
        validator.setUsedParameters(FhirURLValidatorTestData.getUsedParameterMissingRequiredList());
        validator.checkAllRequiredParametersArePresent();
        checkRaisedNotification(Error.class);
    }

    @Test
    public void testExistingModifier() {
        ParameterModifier modifier = validator.getModifierByName(FhirURLValidatorTestData.MODIFIER_CONTAINS, FhirURLValidatorTestData
                .PARAMETER_GIVEN);
        Assert.assertNotNull(modifier);
        Assert.assertEquals(ParameterModifier.CONTAINS, modifier);
    }

    @Test
    public void testUnknownModifier() {
        ParameterModifier modifier = validator.getModifierByName(FhirURLValidatorTestData.UNKNOWN_MODIFIER, FhirURLValidatorTestData.PARAMETER_GIVEN);
        Assert.assertNull(modifier);
        checkRaisedNotification(Error.class);
    }

    @Test
    public void testIncompleteParameter() {
        validator.validateUrlParameter(FhirURLValidatorTestData.INCOMPLETE_PARAMETER);
        checkRaisedNotification(Error.class);
    }

    @Test
    public void testCorrectlyFormattedParameter() {
        validator.validateUrlParameter(FhirURLValidatorTestData.URL1_PARAMETERS);
        Assert.assertEquals(2, validator.getNotifications().size());
    }

    @Test
    public void testCorrectModifier() {
        validator.validateParameterKey(FhirURLValidatorTestData.KEY_WITH_CONTAINS_MODIFIER);
        final List<Object> notifications = validator.getNotifications();
        Assert.assertEquals(2, notifications.size());
        for (Object notification : notifications) {
            Assert.assertEquals(Note.class, notification.getClass());
        }
    }

    @Test
    public void testKeys() {
        validator.validateParameterKey("Test:test");
        final List<Object> notifications = validator.getNotifications();
        Assert.assertEquals(1, notifications.size());
        Assert.assertEquals(Note.class, notifications.get(0).getClass());

    }

    @Test
    public void testBadKeys() {
        validator.validateParameterKey("Tests:test");
        final List<Object> notifications = validator.getNotifications();
        Assert.assertEquals(2, notifications.size());
        Assert.assertEquals(Error.class, notifications.get(0).getClass());

    }
    @Test
    public void testBadKeys2(){
        validator.validateParameterKey("Test:Test:Test");
        final List<Object> notifications = validator.getNotifications();
        Assert.assertEquals(2, notifications.size());
        Assert.assertEquals(Error.class, notifications.get(0).getClass());
        Assert.assertEquals(Error.class, notifications.get(1).getClass());
    }


    @Test
    public void testNotAModifier() {
        validator.validateParameterKey("given:numbers");
        final List<Object> notifications = validator.getNotifications();
        Assert.assertEquals(2, notifications.size());
        Assert.assertEquals(Note.class, notifications.get(0).getClass());

    }

    //   example.com/base/Slot?_include=Slot:schedule&_include:iterate=Schedule:actor&start=ge 2019-01-02&start=le20190106&schedule
    //   .actor:PractitionerRole.specialty=urn:oid:1.2.250.1.213.2.28|SM54&schedule .actor:PractitionerRole.address=Paris&status=free
    @Test
    public void testFullUrl() {
        FhirURLValidatorDescription validatorDescription = new FhirURLValidatorDescription();
        validatorDescription.setResourceName("Slot");
        //validatorDescription.setOperation("ANS-test");
        List<FhirRequestParameter> parameters = validatorDescription.getRequestParameters();
        FhirRequestParameter PARAM_schedule = new FhirRequestParameter("schedule.actor:PractitionerRole.address", ParameterType.STRING, false, null);
        FhirRequestParameter PARAM_START = new FhirRequestParameter("start", ParameterType.STRING, false, null);
        FhirRequestParameter PARAM_STATUS = new FhirRequestParameter("status", ParameterType.STRING, false, null);
        FhirRequestParameter PARAM_scheduleSpe = new FhirRequestParameter("schedule.actor:PractitionerRole.specialty", ParameterType.STRING, false, ".+");
        FhirRequestParameter PARAM_Include = new FhirRequestParameter("_include", ParameterType.STRING, false, null);
        FhirRequestParameter PARAM_IncludeIterate = new FhirRequestParameter("_include:iterate", ParameterType.STRING, false, null);
        parameters.add(PARAM_START);
        parameters.add(PARAM_schedule);
        parameters.add(PARAM_scheduleSpe);
        parameters.add(PARAM_STATUS);
        parameters.add(PARAM_Include);
        parameters.add(PARAM_IncludeIterate);


        validator.validateRequest(FhirURLValidatorTestData.URL3_FULL, validatorDescription);
        final List<Object> notifications = validator.getNotifications();
        Assert.assertNotSame(0, notifications.size());
    }



    @Test
    public void testNotAllowedModifier() {
        validator.validateParameterKey(FhirURLValidatorTestData.NUMBER_WITH_CONTAINS);
        final List<Object> notifications = validator.getNotifications();
        Assert.assertEquals(Note.class, notifications.get(0).getClass());
        Assert.assertEquals(Error.class, notifications.get(1).getClass());
    }

    @Test
    public void testParameterList() {
        validator.validateUrlParameters(FhirURLValidatorTestData.URL2_PARAMETERS);
        final List<Object> notifications = validator.getNotifications();
        Assert.assertNotSame(0, notifications.size());
    }

    @Test
    public void testValidDomain() {
        validator.validateDomain(FhirURLValidatorTestData.VALID_DOMAIN);
        checkRaisedNotification(Note.class);
    }

    @Test
    public void testLocalhost() {
        validator.validateDomain(FhirURLValidatorTestData.LOCALHOST);
        checkRaisedNotification(Note.class);
    }

    @Test
    public void koTestDomain() {
        validator.validateDomain(FhirURLValidatorTestData.INVALID_DOMAIN);
        checkRaisedNotification(Error.class);
    }

    @Test
    public void testCheckAllowedOperation() {
        validator.checkOperation("$ihe-pix");
        checkRaisedNotification(Note.class);
    }

    @Test
    public void koTestCheckOperation() {
        validator.checkOperation("$ihe-pdqm");
        checkRaisedNotification(Error.class);
    }

    @Test
    public void testProtocolHttp() {
        String url = validator.checkProtocolAndReturnUrl(FhirURLValidatorTestData.HTTP_URL);
        Assert.assertNotNull(url);
        Assert.assertEquals(FhirURLValidatorTestData.URL_NO_PROTOCOL, url);
    }

    @Test
    public void testProtocolHttps() {
        String url = validator.checkProtocolAndReturnUrl(FhirURLValidatorTestData.HTTPS_URL);
        Assert.assertNotNull(url);
        Assert.assertEquals(FhirURLValidatorTestData.URL_NO_PROTOCOL, url);
    }

    @Test
    public void koTestProtocol() {
        String url = validator.checkProtocolAndReturnUrl(FhirURLValidatorTestData.URL_NO_PROTOCOL);
        Assert.assertNull(url);
        checkRaisedNotification(Error.class);
    }

    @Test
    public void testNotValidDomain() {
        boolean result = validator.validateBaseUrlParts(FhirURLValidatorTestData.LOCALHOST, 0);
        Assert.assertEquals(false, result);
    }

    @Test
    public void testValidUrlPart() {
        boolean result = validator.validateBaseUrlParts("fhir", 2);
        Assert.assertEquals(false, result);
    }

    @Test
    public void testDetectResourceName() {
        boolean result = validator.validateBaseUrlParts("Patient", 3);
        Assert.assertEquals(true, true);
    }

    @Test
    public void testValidateUrlWrongResource() {
        validator.validateBaseUrl(FhirURLValidatorTestData.URL_INVALID_RESOURCE);
        int countErrors = 0;
        for (Object notification : validator.getNotifications()) {
            if (notification instanceof Error) {
                countErrors++;
            }
        }
        Assert.assertEquals(1, countErrors);
    }

    @Test
    public void testValidatorUrlCorrectResource() {
        validator.validateBaseUrl(FhirURLValidatorTestData.BASE_URL_PIXM);
        int countErrors = 0;
        for (Object notification : validator.getNotifications()) {
            if (notification instanceof Error) {
                countErrors++;
            }
        }
        Assert.assertEquals(0, countErrors);
    }

    @Test
    public void testValidatorUrlIncorrectOperation() {
        validator.validateBaseUrl(FhirURLValidatorTestData.URL_WRONG_OPERATION);
        int countErrors = 0;
        for (Object notification : validator.getNotifications()) {
            if (notification instanceof Error) {
                countErrors++;
            }
        }
        Assert.assertEquals(1, countErrors);
    }

    @Test
    public void testValidatorUrlDocumentReferenceWithInstantCorrect() {
        FhirURLValidatorDescription validatorDescription = new FhirURLValidatorDescription();
        validatorDescription.setResourceName("DocumentReference");
        List<FhirRequestParameter> parameters = validatorDescription.getRequestParameters();
        FhirRequestParameter PARAM_elements = new FhirRequestParameter("_elements", ParameterType.STRING, true, null);
        FhirRequestParameter PARAM_type = new FhirRequestParameter("type", ParameterType.TOKEN, true, null);
        FhirRequestParameter PARAM_lastUpdated = new FhirRequestParameter("_lastUpdated:gt", ParameterType.INSTANT, true, null);
        parameters.add(PARAM_elements);
        parameters.add(PARAM_type);
        parameters.add(PARAM_lastUpdated);
        validator.validateRequest(FhirURLValidatorTestData.DOCUMENT_REFERENCE_WITH_DATE,validatorDescription);
        int countErrors = 0;
        for (Object notification : validator.getNotifications()) {
            if (notification instanceof Error) {
                countErrors++;
            }
        }
        Assert.assertEquals(0, countErrors);
    }



    @Test
    public void testValidatorUrlMissingOperation() {
        validator.validateBaseUrl(FhirURLValidatorTestData.URL_PQDM_READ);
        int countErrors = 0;
        for (Object notification : validator.getNotifications()) {
            if (notification instanceof Error) {
                countErrors++;
            }
        }
        Assert.assertEquals(1, countErrors);
    }

    @Test
    public void testValidatorUrlWithResourceId() {
        // FIXME: on en rentre pas dans le test pour _id
        validator.getValidatorDescription().setOperation(null);
        validator.validateBaseUrl(FhirURLValidatorTestData.URL_PQDM_READ);
        int countErrors = 0;
        for (Object notification : validator.getNotifications()) {
            if (notification instanceof Error) {
                countErrors++;
            }
        }
        Assert.assertEquals(0, countErrors);
    }

    @Test
    public void testTokenRegexSystemOnly() {
        validator.validateParameterValueFormat(FhirURLValidatorTestData.TOKEN_SYSTEM_ONLY, CommonFhirRequestParameter.ID);
        checkRaisedNotification(Note.class);
    }

    @Test
    public void testTokenRegexCodeOnly() {
        validator.validateParameterValueFormat(FhirURLValidatorTestData.TOKEN_CODE_ONLY, CommonFhirRequestParameter.ID);
        checkRaisedNotification(Note.class);
    }

    @Test
    public void testTokenRegexSystemCode() {
        validator.validateParameterValueFormat(FhirURLValidatorTestData.TOKEN_SYSTEM_CODE, CommonFhirRequestParameter.ID);
        checkRaisedNotification(Note.class);
    }

    @Test
    public void testTokenRegexSystemNoCode() {
        validator.validateParameterValueFormat(FhirURLValidatorTestData.TOKEN_CODE_NO_SYSTEM, CommonFhirRequestParameter.ID);
        checkRaisedNotification(Note.class);
    }

    @Test
    public void testTokenCodeIsURI() {
        validator.validateParameterValueFormat(FhirURLValidatorTestData.TOKEN_CODE_URI, CommonFhirRequestParameter.ID);
        checkRaisedNotification(Note.class);
    }

    @Test
    public void testTokenNotAValidSystem() {
        validator.validateParameterValueFormat(FhirURLValidatorTestData.TOKEN_INVALID_SYSTEM, CommonFhirRequestParameter.ID);
        checkRaisedNotification(Error.class);
    }

    @Test
    public void testValidateRequest() {
        validator.validateRequest(FhirURLValidatorTestData.URL1_FULL, FhirURLValidatorTestData.getValidatorDescriptionForTest());
        final List<Object> notifications = validator.getNotifications();
        Assert.assertNotSame(0, notifications.size());
    }

    //@Test
    public void testValidateEmptyRequest() {
        validator.validateRequest("", FhirURLValidatorTestData.getValidatorDescriptionForTest());
        checkRaisedNotification(Error.class);
    }

    //@Test
    public void testValidateNoParameterRequest() {
        validator.validateRequest(FhirURLValidatorTestData.RETRIEVE_PATIENT_REQUEST, FhirURLValidatorTestData.getValidatorDescriptionForTest());
        final List<Object> notifications = validator.getNotifications();
        Assert.assertNotSame(0, notifications.size());
    }

    //@Test
    public void testValidateTooManyParametersRequest() {
        validator.validateRequest(FhirURLValidatorTestData.TOO_MANY_QUESTION_MARKS_REQUEST, FhirURLValidatorTestData.getValidatorDescriptionForTest
                ());
        checkRaisedNotification(Error.class);
    }

    private void checkRaisedNotification(Class expectedClass) {
        Assert.assertEquals(1, validator.getNotifications().size());
        Object notification = validator.getNotifications().get(0);
        if (notification instanceof Notification) {
            Assert.assertEquals(((Notification) notification).getDescription(), expectedClass, notification.getClass());
        }
    }
}